from shop2 import app, db, Technology

products = [
    { "name": "Hat", "price": "£9.99", "description": "Heavy cotton bucket hat with welsh colours and dragon.", "image": "/BucketHat.jpg", "eiikg": "13.3" },
    { "name": "Shirt", "price": "£29.99", "description": "Heavy cotton rugby shirt with welsh colours, sponsors and badge.", "image": "/RugbyShirt.jpg", "eiikg": "2.3" },
    { "name": "Daffodil", "price": "£4.99", "description": "Inflattable daffodil that represents nation flower of Wales.", "image": "/InflatableDaffodil.jpg", "eiikg": "1.6" },
    { "name": "Leek", "price": "£4.99", "description": "Inflatable Leek that represents some Welsh culture.", "image": "/InflatableLeek.jpg", "eiikg": "1.5" },

]

with app.app_context():
    db.create_all()

    for prod in products:
        newProd = Technology(name=prod["name"], price=prod["price"], description=prod["description"], image=prod["image"], eiikg=prod["eiikg"])
        db.session.add(newProd)
    