from flask import Flask, render_template, request, jsonify, url_for, session, redirect, request
from flask_wtf import FlaskForm
from wtforms import SubmitField, IntegerField, StringField
from wtforms.validators import DataRequired, NumberRange, Length, ValidationError
from flask_bootstrap import Bootstrap
from flask_sqlalchemy import SQLAlchemy


app = Flask(__name__)
bootstrap = Bootstrap(app)
app.config['SECRET_KEY'] = "this is my secret password"
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///data.sqlite3'
db = SQLAlchemy(app)

products = [
    {"id": 1, "name": "Hat", "price":9.99},
    {"id": 2, "name": "Shirt", "price":29.99},
    {"id": 3, "name": "Daffodil", "price":4.99},
    {"id": 4, "name": "Leek", "price":4.99}
]

payment_status = None




class Technology(db.Model):
    __tablename__ = 'products'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.Text, index=True, unique=True)
    price = db.Column(db.Text)
    description = db.Column(db.Text)
    image = db.Column(db.Text)
    eiikg = db.Column(db.Text)


class OpinionForm(FlaskForm):
    opinion = StringField('Please tell us your intial thoughts on this product, it helps up to improve our store and servies:', validators=[DataRequired()])
    submit = SubmitField('Submit')


@app.before_request
def clear_session():
    if 'basket' not in session and not request.path.startswith('/static'):
        session['basket'] = []



@app.route('/', methods=['GET', 'POST'])
def galleryPage():
    sort_option = request.form.get('sort_option')
    if sort_option == 'name':
        products = Technology.query.order_by(Technology.name).all()
    elif sort_option == 'price_asc':
        products = sorted(Technology.query.all(), key=lambda x: float(x.price.replace('£', '')))
    elif sort_option == 'price_desc':
        products = sorted(Technology.query.all(), key=lambda x: float(x.price.replace('£', '')), reverse=True)
    elif sort_option == 'EnvImpact_asc':
        products = sorted(Technology.query.all(), key=lambda x: float(x.eiikg))
    elif sort_option == 'EnvImpact_desc':
        products = sorted(Technology.query.all(), key=lambda x: float(x.eiikg), reverse=True)
    else:
        products = Technology.query.all()
    return render_template('index.html', products=products)





@app.route('/product/<int:techId>', methods=['GET', 'POST'])
def singleProductPage(techId):
    product = Technology.query.get(techId)
    if product:
        form = OpinionForm()
        if form.validate_on_submit():
            return render_template('random.html', product=product, opinion=form.opinion.data)
        else:
            return render_template('SingleTech.html', product=product, form=form)
    else:
        return render_template('product_not_found.html')


@app.route('/add_to_basket/<int:techId>', methods=['POST'])
def add_to_basket(techId):
    product = Technology.query.get(techId)

    if product:
        basket = session.get('basket', [])
        for item in basket:
            if item['id'] == product.id:
                item['quantity'] += 1
                session['basket'] = basket
                return render_template('quantity.html', basket=basket)

        # Append product details including image to the basket
        basket.append({
            "id": product.id,
            "name": product.name,
            "price": product.price,
            "quantity": 1,
            "image": url_for('static', filename='Images/' + product.image)  # Add image field to the basket item
        })
        session['basket'] = basket
        return render_template('add_to_basket.html', basket=basket)

    else:
        return render_template('product_not_found.html')


@app.route('/view_basket')
def view_basket():
    basket = session.get('basket', [])
    total_price = sum(float(item['price'][1:]) * item['quantity'] for item in basket)
    total_price = round(total_price, 2)


    basket_with_images = []

    for item in basket:
        product = Technology.query.get(item['id'])
        if product:
            basket_with_images.append({
                'id': product.id,
                'name': product.name,
                'price': product.price,
                'quantity': item['quantity'],
                'image_url': url_for('static', filename='Images/' + product.image)
            })

    return render_template('view_basket.html', basket=basket_with_images, total_price=total_price)


@app.route('/remove_from_basket/<int:techId>', methods=['POST'])
def remove_from_basket(techId):
    basket = session.get('basket', [])
    for item in basket:
        if item['id'] == techId:
            basket.remove(item)
            session['basket'] = basket
            return render_template('remove_from_basket.html', basket=basket)

    return render_template('product_not_found.html')


@app.route('/clear_basket', methods=['POST'])
def clear_basket():
    products = Technology.query.all()
    session.pop('basket', None)
    return render_template('clear_basket.html', products = products)


@app.route('/checkout', methods=['GET', 'POST'])
def checkout():
    print("Entering checkout route...")

    # Retrieve basket from session
    basket = session.get('basket', [])

    # Calculate total price
    total_price = sum(float(item['price'][1:]) * item['quantity'] for item in basket)
    total_price = round(total_price, 2)

    if request.method == 'POST':
        # Handle form submission here
        session['payment_status'] = 'Success'
        session.pop('basket', None)  # Clear the basket after successful payment
        #return redirect(url_for('order_confirmation'))  # Redirect to a separate route for order confirmation

    # If it's a GET request, simply render the checkout page
    return render_template('checkout.html', basket=basket, total_price=total_price)








@app.route('/submit_order', methods=['POST'])
def submit_order():
    session['payment_status'] = 'Success'
    session.pop('basket', None)
    return render_template('order_confirmation.html')

@app.route('/get_description/<int:techId>', methods=['GET'])
def get_description(techId):
    products = Technology.query.get(techId)
    if products:
        return jsonify(description=products.description)
    else:
        return jsonify(error='Item not found'), 404



if __name__ == '__main__':
    app.run(debug=True)
